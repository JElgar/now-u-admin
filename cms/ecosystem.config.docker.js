module.exports = {
  apps: [
    {
      name: 'strapi',
      cwd: '/cms',
      script: 'npm',
      args: 'run develop',
      env: {
        NODE_ENV: 'development',
        DATABASE_HOST: 'postgres', // database endpoint
        DATABASE_PORT: '5432',
        DATABASE_NAME: 'nowucms', // DB name
        DATABASE_USERNAME: 'nowucmsadmin', // your username for psql
        DATABASE_PASSWORD: 'adminsecret', // your password for psql
      },
    },
  ],
};
