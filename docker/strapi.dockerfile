FROM node:latest

COPY ./cms cms
WORKDIR /cms
RUN cp ecosystem.config.docker.js ecosystem.config.js
RUN npm i
RUN npm i pg
RUN npm i -g pm2
RUN npm run build

ENTRYPOINT pm2-dev start ecosystem.config.js
