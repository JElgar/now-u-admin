FROM node:13.12.0-alpine as build

# Use Strapi to build the things
WORKDIR /app

COPY package.json ./
RUN npm i
RUN npm i pg
RUN npm i -g pm2

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

COPY . ./
RUN npm run build


# Build nginx container to host built strapi stuff
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html/

COPY --from=build /app/nginx/strapi.conf /etc/nginx/sites-available/strapi.conf
COPY --from=build /app/nginx/upstream.conf /etc/nginx/conf.d/upstream.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
